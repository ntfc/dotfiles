###############################
# Exports
###############################

typeset -U path
# at the end of this file we export PATH, since the path variable is
# incremented throughout this file
path+=(
  "$HOME/.scripts/"
)

# by default: export WORDCHARS='*?_-.[]~=/&;!#$%^(){}<>'
# we take out the slash, period, angle brackets, dash here.
export WORDCHARS='*?[]~=&;!#$%^(){}<>'

# Browser
export BROWSER='firefox'
# editor
export VISUAL='vim'
export EDITOR='vim'

# configure virtualenvwrapper
if hash virtualenvwrapper.sh 2>/dev/null; then
  export WORKON_HOME=~/.local/share/virtualenvs/
  source virtualenvwrapper.sh
fi

# to set up GPG
export GPG_TTY=$(tty)

# set homebrew related path
if [ -n "${HOMEBREW_PREFIX+x}" ]; then
  path+=(
    $HOMEBREW_PREFIX/opt/inetutils/libexec/gnubin
  )
fi

# set tfenv
if hash tfenv 2>/dev/null; then
  [[ "$(uname -m)" == "arm64" ]] && export TFENV_ARCH=amd64
fi

# NOTE: this must be the last instruction!
export PATH
