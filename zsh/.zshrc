# I'm not using any plugins, so disable it for now
#if [[ -f ~/.zsh/antigen/antigen.zsh ]]; then
#  source ~/.zsh/antigen/antigen.zsh
#
#fi

if [[ $(uname) == "Linux" ]]; then
  for config in ~/.zsh/linux/*.zsh; do
    source "$config"
  done
elif [[ $(uname) == "Darwin" ]]; then
  for config in ~/.zsh/darwin/*.zsh; do
    source "$config"
  done
fi
